# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Invoice',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Fakturierung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides timeline features for invoices
''',
    'description_de_DE': '''
    - Passt die Fakturierung an die Güligkeitsdauermodule der Buchhaltung an.
''',
    'depends': [
        'account_invoice',
        'account_invoice_product_rule',
        'account_timeline',
        'account_timeline_product_rule',
    ],
    'xml': [
        'invoice.xml',
    ],
    'translation': [
    ],
}
