# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
#import datetime
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class RuleLine(ModelSQL, ModelView):
    _name = 'account.tax.rule.line'

    def match_hook(self, line, pattern, field):
        fiscalyear_obj = Pool().get('account.fiscalyear')

        res = super(RuleLine, self).match_hook(line, pattern, field)
        if res:
            # check for valid taxation_method
            fiscalyear = fiscalyear_obj.get_fiscalyear(
                date=pattern['effective_date'])
            if field == 'taxation_method':
                if line['taxation_method'] != fiscalyear.taxation_method:
                    return False
            # Moved to base modul account_timeline, remove this in a future
            # working version.
            ## check for valid date
            #if field == 'effective_date':
            #    if isinstance(pattern[field], datetime.datetime):
            #        pattern[field] = pattern[field].date()
            #    if line['valid_from'] and line['valid_from'] > pattern[field]:
            #        return False
            #    if line['valid_to'] and line['valid_to'] < pattern[field]:
            #        return False
        return res

RuleLine()
