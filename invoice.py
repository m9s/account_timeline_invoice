# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Get, Eval, PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool


class Invoice(ModelSQL, ModelView):
    _name = 'account.invoice'

    effective_date = fields.Function(fields.Date('Effective Date'),
            'get_effective_date')

    def __init__(self):
        super(Invoice, self).__init__()

        self.lines = copy.copy(self.lines)
        if self.lines.context is None:
            self.lines.context = {'effective_date': Eval('effective_date')}
        if self.lines.on_change is None:
            self.lines.on_change = []
        if 'effective_date' not in self.lines.on_change:
            self.lines.on_change += ['effective_date']
        if 'effective_date' not in self.lines.depends:
            self.lines.depends = copy.copy(self.lines.depends)
            self.lines.depends.append('effective_date')

        self.taxes = copy.copy(self.taxes)
        if self.taxes.on_change is None:
            self.taxes.on_change = []
        if 'effective_date' not in self.taxes.on_change:
            self.taxes.on_change += ['effective_date']
        if 'effective_date' not in self.taxes.depends:
            self.taxes.depends = copy.copy(self.taxes.depends)
            self.taxes.depends.append('effective_date')

        self.invoice_date = copy.copy(self.invoice_date)
        if self.invoice_date.on_change is None:
            self.invoice_date.on_change = []
        for value in ['party', 'type']:
            if value not in self.invoice_date.on_change:
                self.invoice_date.on_change += [value]

        self.accounting_date = copy.copy(self.accounting_date)
        if self.accounting_date.on_change is None:
            self.accounting_date.on_change = []
        for value in ['party', 'type']:
            if value not in self.accounting_date.on_change:
                self.accounting_date.on_change += [value]

        attrs = {
            'on_change': [
                'effective_date',
                'accounting_date',
                'invoice_date'
                ],
            'depends': ['effective_date']
            }

        fields = {
            'invoice_date': attrs,
            'accounting_date': attrs,
            'party': attrs,
            }

        for field in fields:
            if not hasattr(self, field):
                continue
            setattr(self, field, copy.copy(getattr(self, field)))
            self_field = getattr(self, field)
            for attribute in fields[field]:
                if getattr(self_field, attribute) is None:
                    setattr(self_field, attribute, fields[field][attribute])
                    continue
                for value in fields[field][attribute]:
                    if value not in getattr(self_field, attribute):
                        new_value = getattr(self_field, attribute) + [value]
                        setattr(self_field, attribute, new_value)

        '''
        alternative: repeating but more readable

        self.invoice_date = copy.copy(self.invoice_date)
        if self.invoice_date.on_change is None:
            self.invoice_date.on_change = []
        for value in ['effective_date', 'accounting_date', 'invoice_date']:
            if value not in self.invoice_date.on_change:
                self.invoice_date.on_change += [value]
        if self.invoice_date.depends is None:
            self.invoice_date.depends = []
        if 'effective_date' not in self.invoice_date.depends:
            self.invoice_date.depends += ['effective_date']

        self.accounting_date = copy.copy(self.accounting_date)
        if self.accounting_date.on_change is None:
            self.accounting_date.on_change = []
        for value in ['effective_date', 'accounting_date', 'invoice_date']:
            if value not in self.accounting_date.on_change:
                self.accounting_date.on_change += [value]
        if self.accounting_date.depends is None:
            self.accounting_date.depends = []
        if 'effective_date' not in self.accounting_date.depends:
            self.accounting_date.depends += ['effective_date']
        '''

        self._rpc.update({
            'on_change_invoice_date': False,
            'on_change_accounting_date': False,
        })

        self._reset_columns()

    def default_effective_date(self):
        date_obj = Pool().get('ir.date')
        return date_obj.today()

    def on_change_party(self, vals):
        context = {}
        if vals.get('effective_date'):
            context['effective_date'] = vals['effective_date']
        with Transaction().set_context(**context):
            res = super(Invoice, self).on_change_party(vals)
        return res

    def _on_change_lines_taxes(self, vals):
        with Transaction().set_context(effective_date=vals.get(
                'effective_date', False)):
            return super(Invoice, self)._on_change_lines_taxes(vals)

    def on_change_invoice_date(self, vals):
        date_obj = Pool().get('ir.date')
        party_obj = Pool().get('party.party')
        today = date_obj.today()

        res = {}
        if vals.get('accounting_date'):
            res['effective_date'] = vals.get('accounting_date')
        elif vals.get('invoice_date'):
            res['effective_date'] = vals.get('invoice_date')
        else:
            res['effective_date'] = today

        if vals.get('party'):
            with Transaction().set_context(
                    effective_date=res['effective_date']):
                party = party_obj.browse(vals['party'])
                if vals.get('type') in ('out_invoice', 'out_credit_note'):
                    res['account'] = party.account_receivable.id
                elif vals.get('type') in ('in_invoice', 'in_credit_note'):
                    res['account'] = party.account_payable.id
        return res

    def on_change_accounting_date(self, vals):
        return self.on_change_invoice_date(vals)

    def get_effective_date(self, ids, name):
        date_obj = Pool().get('ir.date')
        today = date_obj.today()

        res = {}
        for invoice in self.browse(ids):
            if invoice.accounting_date:
                res[invoice.id] = invoice.accounting_date
            elif invoice.invoice_date:
                res[invoice.id] = invoice.invoice_date
            else:
                res[invoice.id] = today
        return res

    def create_move(self, invoice_id):
        invoice = self.browse(invoice_id)
        with Transaction().set_context(effective_date=invoice.effective_date):
            return super(Invoice, self).create_move(invoice_id)

    def update_taxes(self, ids, exception=False):
        tax_obj = Pool().get('account.invoice.tax')
        currency_obj = Pool().get('currency.currency')

        for invoice in self.browse(ids):
            if invoice.state in ('open', 'paid', 'cancel'):
                continue
            with Transaction().set_context(
                    effective_date=invoice.effective_date):
                computed_taxes = self._compute_taxes(invoice)
            if not invoice.taxes:
                for tax in computed_taxes.values():
                    tax_obj.create(tax)
            else:
                tax_keys = []
                for tax in invoice.taxes:
                    if tax.manual:
                        continue
                    key = (tax.base_code.id, tax.base_sign,
                            tax.tax_code.id, tax.tax_sign,
                            tax.account.id, tax.tax.id)
                    if (not key in computed_taxes) or (key in tax_keys):
                        if exception:
                            self.raise_user_error('missing_tax_line')
                        tax_obj.delete(tax.id)
                        continue
                    tax_keys.append(key)
                    if not currency_obj.is_zero(
                            invoice.currency,
                            computed_taxes[key]['base'] - tax.base):
                        if exception:
                            self.raise_user_error('diff_tax_line')
                        tax_obj.write(tax.id,
                                computed_taxes[key])
                for key in computed_taxes:
                    if not key in tax_keys:
                        if exception:
                            self.raise_user_error('missing_tax_line')
                        tax_obj.create(computed_taxes[key])
        return True

    def pay_invoice(self, invoice_id, amount, journal_id, date,
            description, amount_second_currency=False, second_currency=False):
        '''
        Add a payment to an invoice

        :param invoice_id: the invoice id
        :param amount: the amount to pay
        :param journal_id: the journal id for the move
        :param date: the date of the move
        :param description: the description of the move
        :param amount_second_currency: the amount in the second currenry if one
        :param second_currency: the id of the second currency
        :return: the id of the payment line
        '''
        pool = Pool()
        journal_obj = pool.get('account.journal')
        move_obj = pool.get('account.move')
        period_obj = pool.get('account.period')
        account_obj = pool.get('account.account')

        lines = []
        invoice = self.browse(invoice_id)
        with Transaction().set_context(effective_date=date):
            invoice_account_id = account_obj.get_account_by_date(
                    invoice.account.id, date=date)
        journal = journal_obj.browse(journal_id)

        if invoice.type in ('out_invoice', 'in_credit_note'):
            lines.append({
                'name': description,
                'account': invoice_account_id,
                'party': invoice.party.id,
                'debit': Decimal('0.0'),
                'credit': amount,
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            lines.append({
                'name': description,
                'account': journal.debit_account.id,
                'party': invoice.party.id,
                'debit': amount,
                'credit': Decimal('0.0'),
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            if invoice_account_id == journal.debit_account.id:
                self.raise_user_error('same_debit_account')
            if not journal.debit_account:
                self.raise_user_error('missing_debit_account')
        else:
            lines.append({
                'name': description,
                'account': invoice_account_id,
                'party': invoice.party.id,
                'debit': amount,
                'credit': Decimal('0.0'),
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            lines.append({
                'name': description,
                'account': journal.credit_account.id,
                'party': invoice.party.id,
                'debit': Decimal('0.0'),
                'credit': amount,
                'amount_second_currency': amount_second_currency,
                'second_currency': second_currency,
            })
            if invoice_account_id == journal.debit_account.id:
                self.raise_user_error('same_credit_account')
            if not journal.credit_account:
                self.raise_user_error('missing_credit_account')

        with Transaction().set_context(effective_date=date):
            period_id = period_obj.find(invoice.company.id,
                    date=date)

        move_id = move_obj.create({
            'journal': journal.id,
            'period': period_id,
            'date': date,
            'lines': [('create', x) for x in lines],
            })

        move = move_obj.browse(move_id)

        for line in move.lines:
            if line.account.id == invoice_account_id:
                self.write(invoice.id, {
                    'payment_lines': [('add', line.id)],
                    })
                return line.id
        raise Exception('Missing account')

    def copy(self, ids, default=None):
        account_obj = Pool().get('account.account')

        int_id = False
        if isinstance(ids, (int, long)):
            int_id = True
            ids = [ids]

        if default is None:
            default = {}
        default = default.copy()

        new_ids = []
        for invoice in self.browse(ids):
            default['account'] = account_obj.get_account_by_date(
                invoice.account)
            new_id = super(Invoice, self).copy(invoice.id, default=default)
            new_ids.append(new_id)

        if int_id:
            return new_ids[0]
        return new_ids

    def _credit(self, invoice):
        account_obj = Pool().get('account.account')

        res = super(Invoice, self)._credit(invoice)
        res['account'] = account_obj.get_account_by_date(
                invoice.account.id)
        return res

Invoice()


class Line(ModelSQL, ModelView):
    _name = 'account.invoice.line'

    def __init__(self):
        super(Line, self).__init__()
        self.account = copy.copy(self.account)
        if self.account.domain is None:
            self.account.domain = []
        domain_start_date = ('fiscalyear.start_date', '<=',
                    Get(Eval('_parent_invoice', {}), 'effective_date'))
        domain_end_date = ('fiscalyear.end_date', '>=',
                    Get(Eval('_parent_invoice', {}), 'effective_date'))
        pyson_domain_account = PYSONEncoder().encode(self.account.domain)
        if PYSONEncoder().encode(domain_start_date) not in pyson_domain_account:
            self.account.domain.append(domain_start_date)
        if PYSONEncoder().encode(domain_end_date) not in pyson_domain_account:
            self.account.domain.append(domain_end_date)
        if ['_parent_invoice.effective_date'] not in self.account.depends:
           self.account.depends = copy.copy(self.account.depends)
           self.account.depends.append('_parent_invoice.effective_date')

        self.product = copy.copy(self.product)
        if self.product.on_change is None:
            self.product.on_change = []
        if '_parent_invoice.effective_date' not in self.product.on_change:
            self.product.on_change += ['_parent_invoice.effective_date']
        self._reset_columns()

    def copy(self, ids, default=None):
        account_obj = Pool().get('account.account')

        if isinstance(ids, (int, long)):
            ids = [ids]

        if default is None:
            default = {}
        default = default.copy()

        new_ids = []
        for line in self.browse(ids):
            if line.type == 'line':
                default['account'] = account_obj.get_account_by_date(
                        line.account)
            new_id = super(Line, self).copy(line.id,
                    default=default)
            new_ids.append(new_id)
        return new_ids

    def _credit(self, line):
        account_obj = Pool().get('account.account')

        res = super(Line, self)._credit(line)
        if line.type == 'line':
            res['account'] = account_obj.get_account_by_date(
                    line.account.id)
        return res

    def _get_tax_rule_pattern(self, party, vals):
        res = super(Line, self)._get_tax_rule_pattern(party, vals)
        if vals.get('_parent_invoice.effective_date'):
            res['effective_date'] = vals['_parent_invoice.effective_date']
        return res

    def on_change_product(self, vals):
        pool = Pool()
        product_obj = pool.get('product.product')
        party_obj = pool.get('party.party')
        tax_rule_obj = pool.get('account.tax.rule')

        # TODO: merged from a_i_tax_rule_timeline: could this be made better?
        context = Transaction().context
        if vals.get('_parent_invoice.effective_date'):
            context['effective_date'] = vals['_parent_invoice.effective_date']
        with Transaction().set_context(**context):
            res = super(Line, self).on_change_product(vals)
            party = None
            if vals.get('_parent_invoice.party') or vals.get('party'):
                party = party_obj.browse(vals.get('_parent_invoice.party')
                        or vals.get('party'))
            product = product_obj.browse(vals['product'])

            invoice_type = (vals.get('_parent_invoice.type')
                or vals.get('invoice_type'))
            tax_rule = self._get_tax_rule(party, invoice_type)
            if invoice_type in ('in_invoice', 'in_credit_note'):
                res['taxes'] = []
                pattern = self._get_tax_rule_pattern(party, vals)
                for tax in product.supplier_taxes_used:
                    if tax_rule:
                        tax_ids = tax_rule_obj.apply(tax_rule, tax, pattern)
                        if tax_ids:
                            res['taxes'].extend(tax_ids)
                        continue
                    res['taxes'].append(tax.id)
                if tax_rule:
                    tax_ids = tax_rule_obj.apply(tax_rule, False, pattern)
                    if tax_ids:
                        res['taxes'].extend(tax_ids)
            else:
                res['taxes'] = []
                pattern = self._get_tax_rule_pattern(party, vals)
                for tax in product.customer_taxes_used:
                    if tax_rule:
                        tax_ids = tax_rule_obj.apply(tax_rule, tax, pattern)
                        if tax_ids:
                            res['taxes'].extend(tax_ids)
                        continue
                    res['taxes'].append(tax.id)
                if tax_rule:
                    tax_ids = tax_rule_obj.apply(tax_rule, False, pattern)
                    if tax_ids:
                        res['taxes'].extend(tax_ids)
        return res

    def _get_tax_rule(self, party, invoice_type):
        '''
        Get tax rule

        :param party: the BrowseRecord of the party
        :param invoice_type: a string with the invoice_type
        :return: the id of the tax rule
        '''
        config_obj = Pool().get('account.tax.rule.configuration')
        config = config_obj.browse(config_obj.get_singleton_id())
        res = None
        if invoice_type in ('in_invoice', 'in_credit_note'):
            if party and party.supplier_tax_rule:
                res = party.supplier_tax_rule
            else:
                res = config['default_supplier_tax_rule'].id
        else:
            if party and party.customer_tax_rule:
                res = party.customer_tax_rule
            else:
                res = config['default_customer_tax_rule'].id
        return res

Line()
