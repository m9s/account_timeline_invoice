#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.transaction import Transaction


class Line(ModelSQL, ModelView):
    _name = 'account.move.line'

    def default_fiscalyear(self):
        date = Transaction().context.get('effective_date', False)
        with Transaction().set_context(date=date):
            return super(Line, self).default_fiscalyear()

Line()
